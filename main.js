const {Selection, RootNode, Artboard, Color} = require("scenegraph");

/**
 * @param {Selection} selection
 * @param {RootNode} root
 */
function sitemapHandlerFunction(selection, root) {
    let artboard = new Artboard();
    artboard.name = "サイトマップ";
    artboard.width = 100;
    artboard.height = 100;
    artboard.fill = new Color("white");

    root.addChild(artboard);
}

module.exports = {
    commands: {
        createSitemap: sitemapHandlerFunction
    }
};
